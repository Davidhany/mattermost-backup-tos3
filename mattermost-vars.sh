# source this file into machine for env vars

# mysql credentials
export MySqlUser=''
export MySqlPassword='' 
export MySqlDBName=''

# config filePath
export matterMostConfigDirPath=''
export matterMostDataDirPath=''
export backUpFileName='mattermost_backup.tar.gz'
# s3 
export bucketName=''
export s3AccessKey=''
export s3SecretKey=''
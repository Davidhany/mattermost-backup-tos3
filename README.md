# Mattermost-backup-ToS3

## setup
  - Execute the cloudformation to get s3 user credentials and bucket name 
  - Edit the mattermost-vars.sh with env vars
  - Register the task into crontab
```
    Ex: 
    SHELL=/bin/bash
    18 23 * * * source /home/daviid/mattermost-vars.sh && /home/daviid/mattermost-Backup.sh 2>>/home/daviid/cronLog.log

```

## todo 
  - change the bulk insert to multipart insert
  - add s3 lifecycle policy to delete backups after 2 days [DONE]
#!/bin/bash

# pull backup from s3 
bucket=${bucketName}
outputFile="./${backUpFileName}"
fileName=${backUpFileName}
resource="/${bucket}/${fileName}"
contentType="application/x-gzip"
dateValue=`date -R`
stringToSign="GET\n\n${contentType}\n${dateValue}\n${resource}"
echo $stringToSign
s3Key=${s3AccessKey}
s3Secret=${s3SecretKey}
signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${s3Secret} -binary | base64`
echo $signature
curl -H "Host: ${bucket}.s3.amazonaws.com" \
    -H "Date: ${dateValue}" \
    -H "Content-Type: ${contentType}" \
    -H "Authorization: AWS ${s3Key}:${signature}" \
    https://${bucket}.s3.amazonaws.com/${fileName} -o $outputFile


backup="./backup"
config="${backup}/config"
data="${backup}/data"


# # Extract compressed file
tar -xzvf $outputFile
gunzip -r $backup/*

# Import db schema
dbFile=$(ls $backup/db_backup_*)
mysql -u ${MySqlUser} -p${MySqlPassword} -e "CREATE DATABASE ${MySqlDBName}"
mysql -u ${MySqlUser} -p${MySqlPassword} ${MySqlDBName} < $dbFile

# Move config & data
rm -rf ${matterMostConfigDirPath} ${matterMostDataDirPath}
cp -r $config ${matterMostConfigDirPath}
cp -r $data ${matterMostDataDirPath}

# remove backup files
rm -rf $backup $outputFile
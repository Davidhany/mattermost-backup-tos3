#!/bin/bash

now="$(date +'%d_%m_%Y_%H_%M_%S')"
tempFolderPath="./backup"
mkdir -p $tempFolderPath
logfile="./backup_logs.txt"

echo "**********************$(date +'%Y_%m')************************" >> "$logfile"

# store database dumb in backup folder 
sqlFileName="db_backup_$now".gz
sqlFilePath="$tempFolderPath/$sqlFileName"
echo "backup database started at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
mysqldump -u ${MySqlUser} -p${MySqlPassword} ${MySqlDBName} 2>>$logfile | gzip > "$sqlFilePath" 
echo "backup database finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"


# cp mattermost config into backup folder
cp -r ${matterMostConfigDirPath} $tempFolderPath/config 2>>$logfile
echo "cp config finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"


# cp mattermost config into backup folder
cp -r ${matterMostDataDirPath} $tempFolderPath/data 2>>$logfile
echo "cp data finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"


# compress backup temp folder
compressFileName=${backUpFileName}
compressFilePath="./$compressFileName"
echo "compress started at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
gzip -r9 $tempFolderPath/data 2>>$logfile
tar -zcf $compressFilePath $tempFolderPath 2>>$logfile
echo "compress finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"


# # upload to s3 
bucket=${bucketName}
resource="/${bucket}/$compressFileName"
contentType="application/x-gzip"
storageClass="x-amz-storage-class:STANDARD_IA"
dateValue=`date -R`
expireDate=`env TZ=GMT date '+%a, %d %b %Y %T %Z' -d+"1 days"`
stringToSign="PUT\n\n${contentType}\n${dateValue}\n${storageClass}\n${resource}"
s3Key=${s3AccessKey}
s3Secret=${s3SecretKey}
signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${s3Secret} -binary | base64` 2>>$logfile
echo "started uploading to s3 at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
curl -v -i -X PUT -T "$compressFilePath" \
          -H "Host: ${bucket}.s3.amazonaws.com" \
          -H "Date: ${dateValue}" \
          -H "Expires: ${expireDate}" \
          -H "${storageClass}" \
          -H "Content-Type: ${contentType}" \
          -H "Authorization: AWS ${s3Key}:${signature}" \
          https://${bucket}.s3.amazonaws.com/${compressFileName}  2>>$logfile
echo "finished uploading to s3 at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"


# remove temp folder and compressed backup 
rm -r $tempFolderPath $compressFileName  2>>$logfile
echo "temp file removed" >> "$logfile"
echo "******************************************************" >> "$logfile"

